import React from "react"

export default function Navbar(){
    return(
        <nav>
        <img className="logo" src="https://upload.wikimedia.org/wikipedia/commons/a/a7/React-icon.svg" alt="react-logo"/>
        <h3>React Facts</h3>
        <h5>React Course - Project1</h5>
        </nav>
    )
}